#include <iostream>
#include <conio.h>
#include "Queue.h"
#include "Stack.h"
#include "UnorderedArray.h"


using namespace std;

void displayTop(Queue<int> queueArray, Stack<int> stackArray)
{
	cout << "Top of the elements: " << endl;
	cout << "Queue: " << queueArray.top() << endl;
	cout << "Stack: " << stackArray.top() << endl;
}

int main() 
{
	int size;
	cout << "Enter size: ";
	cin >> size;

	Queue<int> queue(size);
	Stack<int> stack(size);

	int choice;
	int elementalPush;
	while (true)
	{
		cout << "What do you want to do?" << endl;
		cout << "1 = Push Elements" << endl;
		cout << "2 = Pop Elements" << endl;
		cout << "3 = Print everything then empty set" << endl;
		cout << "CHOICE: ";
		cin >> choice;

		if (choice == 1)
		{
			cout << "\nGive element to push: ";
			cin >> elementalPush;
			queue.push(elementalPush);
			stack.push(elementalPush);
	
			displayTop(queue, stack);
		}
		
		if (choice == 2)
		{
			stack.pop();
			queue.pop();
			cout << "\nQueue: ";
			for (int i = 0; i < queue.getSize(); i++) 
			{
				cout << queue[i] << " ";
			}

			cout << "\nStack: ";
			for (int i = 0; i < stack.getSize(); i++)
			{
				cout << stack[i] << " ";
			}
		}

		if (choice == 3)
		{
			cout << "\nQueue: ";
			for (int i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << " ";
			}

			cout << endl;
			cout << "Stack: ";
			for (int i = 0; i < stack.getSize(); i++)
			{
				cout << stack[i] << " ";
			}
			queue.clear();
			stack.clear();
		}

		_getch();
		system("CLS");
	}

	system("Pause");
}